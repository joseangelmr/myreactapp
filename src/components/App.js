import React, { Component } from 'react';
import axios from 'axios'

//components
import { List } from './../components'


class App extends Component {

    state = {
        data: [],
        error: null
    }

    componentDidMount() {
        axios.get('http://localhost:5000/songs')
            .then(response => this.setState({ data: response.data }))
            .catch(error => this.setState({ error: error.message }))
    }

    render() {

        const { data, error } = this.state

        return (
            <div className='wrapper-app'>
                {
                    error ? error : <List
                        data={data}
                        onChangeData={(data) => this.setState({ data })}
                        onError={(error) => this.setState({ error })} />
                }
            </div>
        )
    }
}

export default App;