import React, { Component } from 'react';
import axios from 'axios'

class List extends Component {

    state = {}

    componentWillMount() { }

    componentDidMount() { }

    deleteSong = (id) => {

        const { onChangeData, onError } = this.props

        axios.delete(`http://localhost:5000/songs/${id}`)
            .then(response => onChangeData(response.data))
            .catch(error => onError(error.message))

    }

    modifySong = (id, name, artist, album) => {
        const { onChangeData, onError } = this.props
        axios.patch(`http://localhost:5000/songs/${id}`, { name, artist })
            .then(response => onChangeData(response.data))
            .catch(error => onError(error.message))
    }


    renderList = (data) => {
        const songs = data.map((song, i) => {
            return <div key={i} className='song-wrapper' style={{ background: 'lightgrey', margin: 10 }}>
                <div>{'id: '} {song.id}</div>
                <div>{'Nombre: '} {song.name}</div>
                <div>{'Artista: '}{song.artist}</div>
                <div>{'Album: '}{song.album}</div>
                <button onClick={() => this.modifySong(song.id, 'Alonso', 'Parra')}>Modificar</button>
                <button onClick={() => this.deleteSong(song.id)}>Eliminar</button>
            </div>
        })
        return songs
    }


    render() {

        const { data } = this.props

        return (
            <div className='wrapper-list'>
                {this.props.data.length === 0 ? null : this.renderList(data)}
            </div>
        )
    }
}

export default List;